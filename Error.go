package go_ioc

import "errors"

var (
	IOC_ERROR_NOT_PRT      = errors.New("bean must be ptr object")
	IOC_ERROR_NOT_NIL      = errors.New("bean must be not nil")
	IOC_ERROR_MUSBE_STRUCT = errors.New("bean must be struct")

)

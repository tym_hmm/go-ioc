### golang ioc组件


#### 功能说明
* 通过统一容器对资源进行管理
* 每个资源进行一次实例化, 减少对内存的使用
* 支持配置文件批量处理,也支持运行中动态注入
* 自动依赖管理
* 支持interface注入翻转

#### 注意
* 容器注册后会将对象名称及结构体进行同时注册，如名称相同则第二次注册无效
* 在获取时，通过`BeanFactory.GetName`获取时，名称大小写必须与结构体名称完全相同 
* 注入到容器中的结构体必须可以对外进行访问，即首字母必须大写

#### 注入关键
> 通过在结构体中使用tag 进行注入, 见demo使用
```
type OrderService struct {
  Version string
  DB      *DBService `inject:"-"`
}
```

#### 获取方式
```
go get -u gitee.com/tym_hmm/go-ioc
```

#### 使用方式

> 具体使用方式可见demo

1、初始化容器对象
```
var BeanFactory = NewBeanFactory()
```

2、创建配置文件
```
type ServiceConfig struct {
}

func NewServiceConfig() *ServiceConfig {
    return &ServiceConfig{}
}
func (this *ServiceConfig) OrderService() *Services.OrderService {
    return Services.NewOrderService()
}

```
3、对配置文件进行注入
```
 err := BeanFactory.Config(serviceConfig) 
 if err != nil {
    fmt.Println("err", err)
 }
```

4、处理使用中的依赖
```
BeanFactory.Apply(userService) //处理依赖
```

5、操作翻转
```
 //这里 测试 userServices
 userService := NewUserService()
 BeanFactory.Apply(userService) //处理依赖
 fmt.Println(userService.Order.Name())
 fmt.Println(userService.Order.GetOrderInfo(12))
 userService.GetUserInfo(3)
```

6、容器对象获取
```
//通过结构体获取
BeanFactory.Set(Services.NewDBService())

//通过名称获取， 注意, 名称为结构体名称 大小写敏感，
dbService := BeanFactory.GetName("DBService").(*Services.DBService)
if dbService == nil {
	fmt.Println("无法获取")
}

//过对象获取
dbService2 := BeanFactory.Get((*Services.DBService)(nil)).(*Services.DBService)
if dbService2 == nil {
	fmt.Println("无法获取")
}

fmt.Println("dbService =>", dbService.DSN)
fmt.Println("dbService2 =>", dbService2.DSN)
fmt.Printf("beanMapper:%+v\n", BeanFactory.GetBeanContainer().GetBeanMapper())
fmt.Printf("beanNameMapper:%+v\n", BeanFactory.GetBeanContainer().GetBeanNameMapper())

```
package main

import (
	"fmt"
	. "gitee.com/tym_hmm/go-ioc"
	"gitee.com/tym_hmm/go-ioc/Demo/Config"
	"gitee.com/tym_hmm/go-ioc/Demo/Services"
)

var BeanFactory = NewBeanFactory()

func main() {
	serviceConfig := Config.NewServiceConfig()

	err := BeanFactory.Config(serviceConfig)
	if err != nil {
		fmt.Println("err", err)
	}

	////这里 测试 userServices
	//userService := NewUserService()
	//BeanFactory.Apply(userService) //处理依赖
	//fmt.Println(userService.Order.Name())
	//fmt.Println(userService.Order.GetOrderInfo(12))
	//userService.GetUserInfo(3)

	//注册一个服务 这里手动
	BeanFactory.Set(Services.NewDBService())

	//通过名称获取， 注意, 名称为结构体名称 大小写敏感，
	dbService := BeanFactory.GetName("DBService").(*Services.DBService)
	if dbService == nil {
		fmt.Println("无法获取")
	}
	//过对象获取
	dbService2 := BeanFactory.Get((*Services.DBService)(nil)).(*Services.DBService)
	if dbService2 == nil {
		fmt.Println("无法获取")
	}
	fmt.Println("dbService =>", dbService.DSN)
	fmt.Println("dbService2 =>", dbService2.DSN)
	fmt.Printf("beanMapper:%+v\n", BeanFactory.GetBeanContainer().GetBeanMapper())
	fmt.Printf("beanNameMapper:%+v\n", BeanFactory.GetBeanContainer().GetBeanNameMapper())
}

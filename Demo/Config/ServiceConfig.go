package Config

import (
	"gitee.com/tym_hmm/go-ioc/Demo/Services"
)

type ServiceConfig struct {
}

func NewServiceConfig() *ServiceConfig {
	return &ServiceConfig{}
}
func (this *ServiceConfig) OrderService() *Services.OrderService {
	return Services.NewOrderService()
}

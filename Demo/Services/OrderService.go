package Services

import "fmt"

type IOrder interface {
	Name() string
}

type OrderService struct {
	Version string
	DB      *DBService `inject:"-"`
}

func NewOrderService() *OrderService {
	return &OrderService{Version: "1.0"}
}
func (this *OrderService) GetOrderInfo(uid int)string {
	return fmt.Sprintf("获取用户ID %d 的订单信息",uid)
}
func (this *OrderService) Name() string {
	return "order"
}
